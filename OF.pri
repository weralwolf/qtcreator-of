ofx_freamework_pri {
    message("OpenFrameworks files is already included")
} else {

    message("Including OpenFrameworks into project scope")
    #TEMPLATE = app
    #CONFIG -= qt QT core qui console windows

    #DESTDIR = bin
    #OBJECTS_DIR = obj
    #TARGET = GipsyDraws

    #QMAKE_CXXFLAGS_RELEASE += -Ofast -g
    #QMAKE_CXXFLAGS_DEBUG += -Wno-unused-parameter

    #LIBS -= QT core qui console windows
    include(config.pri)

    INCLUDEPATH += \
    /usr/include/atk-1.0 \
    /usr/include/pango-1.0 \
    /usr/include/gio-unix-2.0 \
    /usr/include/glib-2.0 \
    /usr/lib/i386-linux-gnu/glib-2.0/include \
    /usr/lib/x86_64-linux-gnu/glib-2.0/include \
    /usr/include/freetype2 \
    /usr/include/libpng12 \
    /usr/include/gtk-2.0 \
    /usr/lib/gtk-2.0/include \
    /usr/include/cairo \
    /usr/include/gdk-pixbuf-2.0 \
    /usr/include/pixman-1 \
    $$OF_ROOT/libs/FreeImage/include \
    $$OF_ROOT/libs/assimp/include \
    $$OF_ROOT/libs/assimp/include/Compiler \
    $$OF_ROOT/libs/cairo/include \
    $$OF_ROOT/libs/cairo/include/pixman-1 \
    $$OF_ROOT/libs/cairo/include/cairo \
    $$OF_ROOT/libs/cairo/include/libpng15 \
    $$OF_ROOT/libs/fmodex/include \
    $$OF_ROOT/libs/freetype/include \
    $$OF_ROOT/libs/freetype/include/freetype2 \
    $$OF_ROOT/libs/freetype/include/freetype2/freetype \
    $$OF_ROOT/libs/freetype/include/freetype2/freetype/config \
    $$OF_ROOT/libs/freetype/include/freetype2/freetype/internal \
    $$OF_ROOT/libs/freetype/include/freetype2/freetype/internal/services \
    $$OF_ROOT/libs/glew/include \
    $$OF_ROOT/libs/glew/include/GL \
    $$OF_ROOT/libs/kiss/include \
    $$OF_ROOT/libs/portaudio/include \
    $$OF_ROOT/libs/rtAudio/include \
    $$OF_ROOT/libs/tess2/include \
    $$OF_ROOT/libs/poco/include \
    $$OF_ROOT/libs/glu/include -pthread \
    /usr/include/glib-2.0 \
    /usr/lib/i386-linux-gnu/glib-2.0/include \
    /usr/lib/x86_64-linux-gnu/glib-2.0/include \
    /usr/include/gstreamer-0.10 \
    /usr/include/libxml2   \
    $$OF_ROOT/libs/openFrameworks/ \
    $$OF_ROOT/libs/openFrameworks/graphics \
    $$OF_ROOT/libs/openFrameworks/sound \
    $$OF_ROOT/libs/openFrameworks/math \
    $$OF_ROOT/libs/openFrameworks/gl \
    $$OF_ROOT/libs/openFrameworks/communication \
    $$OF_ROOT/libs/openFrameworks/3d \
    $$OF_ROOT/libs/openFrameworks/app \
    $$OF_ROOT/libs/openFrameworks/types \
    $$OF_ROOT/libs/openFrameworks/events \
    $$OF_ROOT/libs/openFrameworks/video \
    $$OF_ROOT/libs/openFrameworks/utils \
    /usr/include/libusb-1.0

    #-g -MMD -MP
    #-Wl,-rpath=

    LIBS += \
    -L./libs \
    -L/usr/lib/i386-linux-gnu  \
    -L/usr/lib/x86_64-linux-gnu \
    -L$$OF_ROOT/libs/freetype/lib/linux \
    -L$$OF_ROOT/libs/kiss/lib/linux \
    -L$$OF_ROOT/libs/openFrameworksCompiled/lib/linux \
    -L$$OF_ROOT/libs/poco/lib/linux \
    -L$$OF_ROOT/libs/portaudio/lib/linux \
    -L$$OF_ROOT/libs/rtAudio/lib/linux \
    -L$$OF_ROOT/libs/tess2/lib/linux \
    -L/usr/lib/i386-linux-gnu \
    -L/usr/lib/x86_64-linux-gnu \
    -L$$OF_ROOT/libs/openFrameworksCompiled/lib/linux/  \
    -L$$OF_ROOT/libs/openFrameworksCompiled/lib/linux64/ -lopenFrameworksDebug \
    -L$$OF_ROOT/libs/freetype/lib/linux/ \
    -L$$OF_ROOT/libs/freetype/lib/linux64/ -lfreetype \
    -L$$OF_ROOT/libs/kiss/lib/linux/ \
    -L$$OF_ROOT/libs/kiss/lib/linux64/ -lkiss \
    -L$$OF_ROOT/libs/portaudio/lib/linux/ \
    -L$$OF_ROOT/libs/portaudio/lib/linux64/ -lportaudio \
    -L$$OF_ROOT/libs/rtAudio/lib/linux/ \
    -L$$OF_ROOT/libs/rtAudio/lib/linux64/ -lRtAudio \
    -L$$OF_ROOT/libs/tess2/lib/linux/ \
    -L$$OF_ROOT/libs/tess2/lib/linux64/ -ltess2 \
    -L$$OF_ROOT/libs/poco/lib/linux/ \
    -L$$OF_ROOT/libs/poco/lib/linux64/ -lPocoNet \
    -lPocoXML \
    -lPocoUtil\
    -lPocoFoundation \
    -L$$OF_ROOT/libs/fmodex/lib/linux \
    -L$$OF_ROOT/libs/fmodex/lib/linux64 \
    -lfmodex \
    -lgtk-x11-2.0\
    -lgdk-x11-2.0\
    -latk-1.0\
    -lgio-2.0\
    -lpangoft2-1.0\
    -lpangocairo-1.0\
    -lgdk_pixbuf-2.0\
    -lm\
    -lcairo\
    -lpango-1.0\
    -lfreetype\
    -lfontconfig\
    -lgobject-2.0\
    -lgmodule-2.0\
    -lgthread-2.0\
    -lrt\
    -lglib-2.0  \
    -lmpg123 \
    -ljack\
    -lpthread\
    -lGLEW\
    -lGLU\
    -lGL\
    -lgstvideo-0.10\
    -lgstapp-0.10\
    -lgstbase-0.10\
    -lgstreamer-0.10\
    -lgobject-2.0\
    -lgmodule-2.0\
    -lxml2\
    -lgthread-2.0\
    -lglib-2.0\
    -ludev\
    -lrt\
    -lcairo  \
    -lglut\
    -lGL\
    -lasound\
    -lopenal\
    -lsndfile\
    -lvorbis\
    -lFLAC\
    -logg\
    -lfreeimage\
    -lGLU \

    CONFIG += ofx_freamework_pri
}
