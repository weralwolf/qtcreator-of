addons_msa_msacore_pri {
    message("MSA/MSACore is already included")
} else {

    message("Including MSA/MSACore addon into project scope")

    msaMSACorePath = $$OF_ROOT/addons/msalibs/MSACore/src

    DEFINES += MSA_HOST_OPENFRAMEWORKS

    SOURCES += \
        $$msaMSACorePath/cinder-lite/Color.cpp \
        $$msaMSACorePath/MSACoreGL.cpp

    HEADERS += \
        $$msaMSACorePath/cinder-lite/ChanTraits.h \
        $$msaMSACorePath/cinder-lite/CinderMath.h \
        $$msaMSACorePath/cinder-lite/Color.h \
        $$msaMSACorePath/cinder-lite/Vector.h \
        $$msaMSACorePath/MSACore.h \
        $$msaMSACorePath/MSACore-Cinder.h \
        $$msaMSACorePath/MSACoreCommon.h \
        $$msaMSACorePath/MSACoreGL.h \
        $$msaMSACorePath/MSACoreMath.h \
        $$msaMSACorePath/MSACore-OF.h

    INCLUDEPATH += \
        $$msaMSACorePath \
        $$msaMSACorePath/cinder-lite

    CONFIG += addons_msa_msacore_pri

}
