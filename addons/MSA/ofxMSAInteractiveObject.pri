addons_msa_ofxmsainteractiveobject_pri {
    message("MSA/ofxMSAInteractiveObject is already included")
} else {

    message("Including MSA/ofxMSAInteractiveObject addon into project scope")

    msaofxMSAInteractiveObjectPath = $$OF_ROOT/addons/msalibs/ofxMSAInteractiveObject/src

    !include(MSACore.pri) {
        error("Can't include MSA/MSACore addon")
    }

    SOURCES += \
        $$msaofxMSAInteractiveObjectPath/ofxMSAInteractiveObject.cpp

    HEADERS += \
        $$msaofxMSAInteractiveObjectPath/ofxMSAInteractiveObject.h

    INCLUDEPATH += \
        $$msaofxMSAInteractiveObjectPath

    CONFIG += addons_msa_ofxmsainteractiveobject_pri
}
