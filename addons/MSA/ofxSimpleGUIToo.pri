addons_msa_ofxsimpleguitoo_pri {
    message("MSA/ofxSimpleGuiToo is already included")
} else {

    message("Including MSA/ofxSimpleGuiToo addon into project scope")

    msaofxSimpleGUITooPath = $$OF_ROOT/addons/msalibs/ofxSimpleGuiToo/src

    !include(../ofxXmlSettings.pri) {
        error("Can't include ofxXMLSettings addon")
    }

    !include(MSACore.pri) {
        error("Can't include MSA/MSACore addon")
    }

    !include(ofxMSAInteractiveObject.pri) {
        error("Can't include MSA/ofxMSAInteractiveObject addon")
    }

    SOURCES += \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiButton.cpp \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiColorPicker.cpp \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiComboBox.cpp \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiContent.cpp \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiFPSCounter.cpp \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiMovieSlider.cpp \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiQuadWarp.cpp \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiSlider2d.cpp \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiTitle.cpp \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiToggle.cpp \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiConfig.cpp \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiControl.cpp \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiPage.cpp \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiToo.cpp \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiValueControl.cpp

    HEADERS += \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiButton.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiColorPicker.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiComboBox.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiContent.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiFPSCounter.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiMovieSlider.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiQuadWarp.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiSlider2d.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiSliderBase.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiSliderFloat.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiSliderInt.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiTitle.h \
        $$msaofxSimpleGUITooPath/Controls/ofxSimpleGuiToggle.h \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiConfig.h \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiControl.h \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiIncludes.h \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiPage.h \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiToo.h \
        $$msaofxSimpleGUITooPath/ofxSimpleGuiValueControl.h

    INCLUDEPATH += \
        $$msaofxSimpleGUITooPath \
        $$msaofxSimpleGUITooPath/Controls

    CONFIG += addons_msa_ofxsimpleguitoo_pri
}
