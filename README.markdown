QtCreator-OF
================
Is an easy way to use [OpenFrameworks](http://www.openframeworks.cc/) things in your project with QMake system.

Usage
================
First of all copy config.pri.tmpl into config.pri and set there relative path to your OpenFrameworks dir.

All you need it's just include OF to your allpication using common [QMake](http://qt-project.org/doc/qt-4.8/qmake-manual.html) syntax:
include("OF.pri")
and do the same for any plugin you want to use.

Currently there small amount of them, but I'll work to make this list 

Contributing
================
I will be happy to hear tips from the more experienced programmers. I would be appreciative if you could contribute pri-files for your plugins or pri-files for special versions of plugins.

To contribute any patches, simply fork this repository using GitHub and send a pull request to me. Thanks!

Contributors
================
Anatolii Koval

Author and maintainer. Writing tests, docs and so on.
[All commits](https://bitbucket.org/weralwolf/qtcreator-of/changesets).
